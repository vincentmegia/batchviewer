﻿using BatchViewer.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatchViewer.Parser
{
    public class JilChainer
    {
        public List<Item> Chain(List<Item> items)
        {
            List<Item> boxes = ChainItem(items);
            return boxes;
        }

        /// <summary>
        /// Performs chaining the boxes to child boxes, child boxes to jobs
        /// </summary>
        /// <param name="items"></param>
        /// <param name="boxes"></param>
        /// <returns></returns>
        private List<Item> ChainItem(List<Item> masterList)
        {
            foreach (Item item in masterList)
            {
                ChainItemRecursively(new List<Item>(new[] {item}), masterList);
            }
            var boxes = from item in masterList
                               where item.ParentBox == null
                               select item;
            return new List<Item>(boxes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="items"></param>
        /// <returns></returns>
        private Item ChainItemRecursively(List<Item> items, List<Item> masterList)
        {
            foreach (Item item in items)
            {
                var childItems = GetChildItems(item, masterList);
                item.Items = new List<Item>(childItems);
                if (childItems.Count > 0)
                    ChainItemRecursively(item.Items, masterList);
            }
            return new Item();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="masterList"></param>
        /// <returns></returns>
        private List<Item> GetChildItems(Item item, List<Item> masterList)
        {
            var childItems = from childItem in masterList
                              where (childItem.ParentBox != null &&
                              childItem.ParentBox.Equals(item) &&
                              !childItem.Equals(item))
                              select childItem;
            return new List<Item>(childItems);
        }
    }
}