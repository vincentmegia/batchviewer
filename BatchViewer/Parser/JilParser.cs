﻿using BatchViewer.Domain;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatchViewer.Parser
{
    public class JilParser
    {
        private List<Item> _items;

        public JilParser()
        {
            _items = new List<Item>();
        }

        /// <summary>
        /// Parses into entity and loads into a flat hashtable
        /// </summary>
        /// <param name="jilFile"></param>
        /// <returns></returns>
        public List<Item> Parse(string jilFile)
        {
            List<Item> items = new List<Item>();
            string[] jilContents = File.ReadAllLines(jilFile);
            Item item = null;
            foreach(string line in jilContents)
            {
                if (line == string.Empty)
                    continue;
                if (line.Contains("/*"))
                {
                    item = new Item();
                    items.Add(item);
                    continue;
                }
                    
                
                if (line.Contains("insert"))
                {
                    //perform a little cleanup
                    string text = line
                        .Replace("insert_job: ", "insert_job:")
                        .Replace("job_type: ", "job_type:")
                        .Replace("\t", " ")
                        .Trim();
                    string[] tokens = text.Split(' ');
                    GetItemName(tokens[0], item);
                    string itemType = GetItemType(tokens);
                    GetItemType(itemType, item);
                    continue;
                }
                if (line.Contains("days_of_week"))
                {
                    ParseDaysInWeek(line, item);
                    continue;
                }
                if (line.Contains("start_times"))
                {
                    ParseStartTimes(line, item);
                    continue;
                }
                if (line.Contains("condition"))
                {
                    
                    ParseCondition(line, item);
                    continue;
                }
                if (line.Contains("box_name"))
                {
                    ParseParentBox(line, item);
                    continue;
                }
            }

            return items;
        }

        /// <summary>
        /// Determines if the first line is a box or job
        /// </summary>
        /// <returns></returns>
        private void GetItemName(string line, Item item)
        {
            string[] tokens = line.Split(' ');
            string itemName = tokens[0].Split(':')[1];
            item.Name = itemName;
        }

        /// <summary>
        /// Determines if the first line is a box or job
        /// </summary>
        /// <returns></returns>
        private void GetItemType(string line, Item item)
        {
            string[] tokens = line.Split(':');
            string itemType = tokens[1];
            //check 4 element if "b" for box else it's a job
            item.Type = (itemType == "b")
                ? ItemType.Box
                : ItemType.Job;
        }

        /// <summary>
        /// 
        /// </summary>
        private void ParseDaysInWeek(string line, Item item)
        {
            string[] tokens = line.Split(':');
            item.DaysInWeek = tokens[1];
        }

        /// <summary>
        /// 
        /// </summary>
        private void ParseStartTimes(string line, Item item)
        {
            string text = line
                        .Replace("start_times:", string.Empty)
                        .Replace("\"", string.Empty);
            string startTime = DateTime.Today.ToString("MMM-dd-yyyy") + (text);
            item.StartTime = DateTime.Parse(startTime);
        }

        /// <summary>
        /// 
        /// </summary>
        private void ParseCondition(string line, Item item)
        {
            string[] tokens = line.Split(':');
            item.Condition = tokens[1];
        }

        private void ParseParentBox(string line, Item item)
        {
            string[] tokens = line.Split(':');
            item.ParentBox = new Item { Name = tokens[1].Trim() };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokens"></param>
        /// <returns></returns>
        private string GetItemType(string[] tokens)
        { 
            string itemType = null;
            for (int index = 1; index <= tokens.Length; index++)
            {
                if (!string.IsNullOrEmpty(tokens[index]))
                {
                    itemType = tokens[index];
                    break;
                }
            }
            return itemType;
        }
    }
}
