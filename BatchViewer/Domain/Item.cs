﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatchViewer.Domain
{
    public class Item
    {
        public string Name { get; set; }
        public string DaysInWeek { get; set; }
        public string Condition { get; set; }
        public string Duration { get; set; }
        public DateTime? StartTime { get; set; }
        public ItemType Type { get; set; }
        public Item ParentBox { get; set; }
        public List<Item> Items { get; set; }

        public Item()
        {
            this.StartTime = null;
            this.Duration = "90:00";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            Item target = (Item)obj;
            if (this.Name == target.Name) return true;
            return base.Equals(obj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("Boxname: {0}", this.Name);
        }
    }
}
