﻿namespace BatchViewer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn1 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn2 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn3 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn4 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn5 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn6 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn7 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn8 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn9 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn10 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn11 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn12 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn13 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn14 = new C1.Win.C1GanttView.TaskPropertyColumn();
            this.c1GanttView1 = new C1.Win.C1GanttView.C1GanttView();
            ((System.ComponentModel.ISupportInitialize)(this.c1GanttView1)).BeginInit();
            this.SuspendLayout();
            // 
            // c1GanttView1
            // 
            this.c1GanttView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.c1GanttView1.BackColor = System.Drawing.SystemColors.Window;
            taskPropertyColumn1.Caption = "Task Mode";
            taskPropertyColumn1.ID = 842276178;
            taskPropertyColumn1.Property = C1.Win.C1GanttView.TaskProperty.Mode;
            taskPropertyColumn2.Caption = "Task Name";
            taskPropertyColumn2.ID = 1497540567;
            taskPropertyColumn2.Property = C1.Win.C1GanttView.TaskProperty.Name;
            taskPropertyColumn3.Caption = "Duration";
            taskPropertyColumn3.ID = 876758113;
            taskPropertyColumn3.Property = C1.Win.C1GanttView.TaskProperty.Duration;
            taskPropertyColumn3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            taskPropertyColumn3.Visible = false;
            taskPropertyColumn4.Caption = "Duration Units";
            taskPropertyColumn4.ID = 757718494;
            taskPropertyColumn4.Property = C1.Win.C1GanttView.TaskProperty.DurationUnits;
            taskPropertyColumn4.Visible = false;
            taskPropertyColumn5.Caption = "Start";
            taskPropertyColumn5.ID = 503407342;
            taskPropertyColumn5.Property = C1.Win.C1GanttView.TaskProperty.Start;
            taskPropertyColumn5.Visible = false;
            taskPropertyColumn6.Caption = "Finish";
            taskPropertyColumn6.ID = 895790434;
            taskPropertyColumn6.Property = C1.Win.C1GanttView.TaskProperty.Finish;
            taskPropertyColumn6.Visible = false;
            taskPropertyColumn7.Caption = "% Complete";
            taskPropertyColumn7.ID = 17897152;
            taskPropertyColumn7.Property = C1.Win.C1GanttView.TaskProperty.PercentComplete;
            taskPropertyColumn7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            taskPropertyColumn7.Visible = false;
            taskPropertyColumn8.Caption = "Constraint Type";
            taskPropertyColumn8.ID = 2130256068;
            taskPropertyColumn8.Property = C1.Win.C1GanttView.TaskProperty.ConstraintType;
            taskPropertyColumn8.Visible = false;
            taskPropertyColumn9.Caption = "Constraint Date";
            taskPropertyColumn9.ID = 378275457;
            taskPropertyColumn9.Property = C1.Win.C1GanttView.TaskProperty.ConstraintDate;
            taskPropertyColumn9.Visible = false;
            taskPropertyColumn10.Caption = "Predecessors";
            taskPropertyColumn10.ID = 2114498125;
            taskPropertyColumn10.Property = C1.Win.C1GanttView.TaskProperty.Predecessors;
            taskPropertyColumn10.Visible = false;
            taskPropertyColumn11.Caption = "Deadline";
            taskPropertyColumn11.ID = 1208145557;
            taskPropertyColumn11.Property = C1.Win.C1GanttView.TaskProperty.Deadline;
            taskPropertyColumn11.Visible = false;
            taskPropertyColumn12.Caption = "Calendar";
            taskPropertyColumn12.ID = 483057771;
            taskPropertyColumn12.Property = C1.Win.C1GanttView.TaskProperty.Calendar;
            taskPropertyColumn12.Visible = false;
            taskPropertyColumn13.Caption = "Resource Names";
            taskPropertyColumn13.ID = 1895479002;
            taskPropertyColumn13.Property = C1.Win.C1GanttView.TaskProperty.ResourceNames;
            taskPropertyColumn13.Visible = false;
            taskPropertyColumn14.Caption = "Notes";
            taskPropertyColumn14.ID = 964419173;
            taskPropertyColumn14.Property = C1.Win.C1GanttView.TaskProperty.Notes;
            taskPropertyColumn14.Visible = false;
            this.c1GanttView1.Columns.Add(taskPropertyColumn1);
            this.c1GanttView1.Columns.Add(taskPropertyColumn2);
            this.c1GanttView1.Columns.Add(taskPropertyColumn3);
            this.c1GanttView1.Columns.Add(taskPropertyColumn4);
            this.c1GanttView1.Columns.Add(taskPropertyColumn5);
            this.c1GanttView1.Columns.Add(taskPropertyColumn6);
            this.c1GanttView1.Columns.Add(taskPropertyColumn7);
            this.c1GanttView1.Columns.Add(taskPropertyColumn8);
            this.c1GanttView1.Columns.Add(taskPropertyColumn9);
            this.c1GanttView1.Columns.Add(taskPropertyColumn10);
            this.c1GanttView1.Columns.Add(taskPropertyColumn11);
            this.c1GanttView1.Columns.Add(taskPropertyColumn12);
            this.c1GanttView1.Columns.Add(taskPropertyColumn13);
            this.c1GanttView1.Columns.Add(taskPropertyColumn14);
            this.c1GanttView1.DefaultWorkingTimes.Interval_1.Empty = false;
            this.c1GanttView1.DefaultWorkingTimes.Interval_1.From = new System.DateTime(1, 1, 1, 9, 0, 0, 0);
            this.c1GanttView1.DefaultWorkingTimes.Interval_1.To = new System.DateTime(1, 1, 1, 13, 0, 0, 0);
            this.c1GanttView1.DefaultWorkingTimes.Interval_2.Empty = false;
            this.c1GanttView1.DefaultWorkingTimes.Interval_2.From = new System.DateTime(1, 1, 1, 14, 0, 0, 0);
            this.c1GanttView1.DefaultWorkingTimes.Interval_2.To = new System.DateTime(1, 1, 1, 18, 0, 0, 0);
            this.c1GanttView1.Location = new System.Drawing.Point(12, 12);
            this.c1GanttView1.Name = "c1GanttView1";
            this.c1GanttView1.Size = new System.Drawing.Size(1023, 530);
            this.c1GanttView1.StartDate = new System.DateTime(2014, 1, 30, 0, 0, 0, 0);
            this.c1GanttView1.TabIndex = 0;
            this.c1GanttView1.Timescale.BottomTier.Format = "i";
            this.c1GanttView1.Timescale.BottomTier.MinWidth = 20;
            this.c1GanttView1.Timescale.BottomTier.Units = C1.Win.C1GanttView.TimescaleUnits.Minutes;
            this.c1GanttView1.Timescale.MiddleTier.Format = "a";
            this.c1GanttView1.Timescale.MiddleTier.Units = C1.Win.C1GanttView.TimescaleUnits.Hours;
            this.c1GanttView1.Timescale.MiddleTier.Visible = true;
            this.c1GanttView1.Timescale.TopTier.Format = "wwww";
            this.c1GanttView1.Timescale.TopTier.Visible = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1047, 554);
            this.Controls.Add(this.c1GanttView1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.c1GanttView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private C1.Win.C1GanttView.C1GanttView c1GanttView1;
    }
}

