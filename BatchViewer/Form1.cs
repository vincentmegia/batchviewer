﻿using BatchViewer.Domain;
using BatchViewer.Parser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BatchViewer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            PopulateGanttView(); 
        }

        private void PopulateGanttView()
        {
            var items = new JilParser().Parse(@"C:\Workspace\Dot Net Project\BatchViewer\BatchViewer\Scaffolding\PMS-JIL.txt");
            var boxes = new JilChainer().Chain(items);

            foreach(var item in boxes)
            {
                CreateTaskRecursively(item);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        private void CreateTaskRecursively(Item item)
        {
            var task = CreateTask(item);
            this.c1GanttView1.Tasks.Add(task);
            foreach (Item childItems in item.Items)
            {
                task = CreateTask(childItems);
                this.c1GanttView1.Tasks.Add(task);
                if (childItems.Items.Count > 0)
                    CreateTaskRecursively(childItems);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="box"></param>
        /// <returns></returns>
        private C1.Win.C1GanttView.Task CreateTask(Item box)
        {
            var task = new C1.Win.C1GanttView.Task
            {
                Name = box.Name,
                Mode = C1.Win.C1GanttView.TaskMode.Automatic,
                Start = box.StartTime,
                Duration = 90.00,
                DurationUnits = C1.Win.C1GanttView.DurationUnits.Minutes
            };

            if (box.ParentBox != null)
            {
                var predecessor = new C1.Win.C1GanttView.Predecessor();
                predecessor.PredecessorTask = this.c1GanttView1.Tasks.Search(box.ParentBox.Name);
                task.Predecessors.Add(predecessor);
            }
            return task;
        }
    }
}
